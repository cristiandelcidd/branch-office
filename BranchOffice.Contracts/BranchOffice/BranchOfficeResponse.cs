namespace BranchOffice.Contracts.BranchOffice;

public record BranchOfficeResponse(
    Guid Id,
    string BranchOfficeName,
    string AdminName,
    string PhoneNumber,
    string Address,
    string Fax,
    int OrderPerMonth,
    DateTime CreatedAt,
    DateTime UpdatedAt
    );