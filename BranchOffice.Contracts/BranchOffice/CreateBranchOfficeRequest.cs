namespace BranchOffice.Contracts.BranchOffice;

public record CreateBranchOfficeRequest(
    string BranchOfficeName,
    string AdminName,
    string PhoneNumber,
    string Address,
    string Fax,
    int OrderPerMonth,
    DateTime CreatedAt,
    DateTime UpdatedAt
);