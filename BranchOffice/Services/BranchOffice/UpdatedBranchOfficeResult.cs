namespace BranchOffice.Services.BranchOffice;

public record struct UpdatedBranchOffice(bool isNewlyCreated);