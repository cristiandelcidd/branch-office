using BranchOffice.Models;
using ErrorOr;
using BranchOffice.ServiceErrors;

namespace BranchOffice.Services.BranchOffice;

public class BranchOfficeService : IBranchOfficeService
{
    private static readonly Dictionary<Guid, BranchOfficeModel> _branchOffices = new();

    public ErrorOr<Created> CreateBranchOffice(BranchOfficeModel branchOffice)
    {
        _branchOffices.Add(branchOffice.Id, branchOffice);

        return Result.Created;
    }

    public ErrorOr<Deleted> DeleteBranchOffice(Guid id)
    {
        _branchOffices.Remove(id);

        return Result.Deleted;
    }

    public ErrorOr<BranchOfficeModel> GetBranchOffice(Guid id)
    {
        if (_branchOffices.TryGetValue(id, out var branchOffice))
        {
            return branchOffice;
        }
        return Errors.BranchOffice.NotFound;
    }

    public ErrorOr<UpdatedBranchOffice> UpdateBranchOffice(BranchOfficeModel branchOffice)
    {
        var isNewlyCreated = !_branchOffices.ContainsKey(branchOffice.Id);
        _branchOffices[branchOffice.Id] = branchOffice;

        return new UpdatedBranchOffice(isNewlyCreated);
    }
}