using BranchOffice.Models;
using ErrorOr;

namespace BranchOffice.Services.BranchOffice;

public interface IBranchOfficeService
{
    ErrorOr<Created> CreateBranchOffice(BranchOfficeModel branchOffice);
    ErrorOr<BranchOfficeModel> GetBranchOffice(Guid id);
    ErrorOr<UpdatedBranchOffice> UpdateBranchOffice(BranchOfficeModel branchOffice);
    ErrorOr<Deleted> DeleteBranchOffice(Guid id);
}