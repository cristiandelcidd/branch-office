using Microsoft.AspNetCore.Mvc;
using ErrorOr;

using BranchOffice.Contracts.BranchOffice;
using BranchOffice.Models;
using BranchOffice.Services.BranchOffice;

namespace BranchOffice.Controllers;


public class BranchOfficeController : ApiController
{
    private readonly IBranchOfficeService _branchOfficeService;

    public BranchOfficeController(IBranchOfficeService branchOfficeService)
    {
        _branchOfficeService = branchOfficeService;
    }

    [HttpPost]
    public IActionResult CreateBranchOffice(CreateBranchOfficeRequest request)
    {
        ErrorOr<BranchOfficeModel> requestToBranchOfficeResult = BranchOfficeModel.From(request);

        if (requestToBranchOfficeResult.IsError)
        {
            return Problem(requestToBranchOfficeResult.Errors);
        }

        var branchOffice = requestToBranchOfficeResult.Value;

        ErrorOr<Created> createBranchOfficeResult = _branchOfficeService.CreateBranchOffice(branchOffice);

        return createBranchOfficeResult.Match(
            create => CreatedAtGetBranchOffice(branchOffice),
            errors => Problem(errors)
        );
    }

    [HttpGet("{id:guid}")]
    public IActionResult GetBranchOffice(Guid id)
    {
        ErrorOr<BranchOfficeModel> getBranchOfficeResult = _branchOfficeService.GetBranchOffice(id);

        return getBranchOfficeResult.Match(branchOffice => Ok(MapBranchOfficeResponse(branchOffice)), errors => Problem(errors));

        // if (getBranchOfficeResult.IsError && getBranchOfficeResult.FirstError == Errors.BranchOffice.NotFound)
        // {
        //     return NotFound();
        // }

        // var branchOffice = getBranchOfficeResult.Value;
        // BranchOfficeResponse response = MapBranchOfficeResponse(branchOffice);

        // return Ok(response);
    }

    [HttpPut("{id:guid}")]
    public IActionResult UpdateBranchOffice(Guid id, UpdateBranchOfficeRequest request)
    {
        ErrorOr<BranchOfficeModel> requestToBranchOfficeResult = BranchOfficeModel.From(id, request);

        if (requestToBranchOfficeResult.IsError)
        {
            return Problem(requestToBranchOfficeResult.Errors);
        }

        var branchOffice = requestToBranchOfficeResult.Value;

        ErrorOr<UpdatedBranchOffice> updateBranchOfficeResult = _branchOfficeService.UpdateBranchOffice(branchOffice);

        return updateBranchOfficeResult.Match(updated => updated.isNewlyCreated ? CreatedAtGetBranchOffice(branchOffice) : NoContent(), errors => Problem(errors));
    }

    [HttpDelete("{id:guid}")]
    public IActionResult DeleteBranchOffice(Guid id)
    {
        ErrorOr<Deleted> deleteBranchOfficeResult = _branchOfficeService.DeleteBranchOffice(id);

        return deleteBranchOfficeResult.Match(deleted => NoContent(), errors => Problem(errors));
    }

    private static BranchOfficeResponse MapBranchOfficeResponse(BranchOfficeModel branchOffice)
    {
        return new BranchOfficeResponse(branchOffice.Id, branchOffice.BranchOfficeName!, branchOffice.AdminName!, branchOffice.PhoneNumber!, branchOffice.Address!, branchOffice.Fax!, branchOffice.OrderPerMonth, branchOffice.CreatedAt, branchOffice.UpdatedAt);
    }

    private IActionResult CreatedAtGetBranchOffice(BranchOfficeModel branchOffice)
    {
        return CreatedAtAction(actionName: nameof(GetBranchOffice), routeValues: new { id = branchOffice.Id }, value: MapBranchOfficeResponse(branchOffice));
    }
}