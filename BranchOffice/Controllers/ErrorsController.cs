using Microsoft.AspNetCore.Mvc;

namespace BranchOffice.Controllers;

public class ErrorsController : ControllerBase
{
    [Route("/error")]
    public IActionResult Error()
    {
        return Problem();
    }
}