using ErrorOr;
using BranchOffice.ServiceErrors;
using BranchOffice.Contracts.BranchOffice;

namespace BranchOffice.Models;

public class BranchOfficeModel
{
    public const int MinBranchOfficeNameLength = 3;
    public const int MaxBranchOfficeNameLength = 50;
    public const int MinAdminNameLength = 3;
    public const int MaxAdminNameLength = 50;
    public Guid Id
    { get; }
    public string? BranchOfficeName { get; }
    public string? AdminName { get; }
    public string? PhoneNumber { get; }
    public string? Address { get; }
    public string? Fax { get; }
    public int OrderPerMonth { get; }
    public DateTime CreatedAt { get; }
    public DateTime UpdatedAt { get; }

    private BranchOfficeModel(Guid id, string branchOfficeName, string adminName, string phoneNumber, string address, string fax, int orderPerMonth, DateTime createdAt, DateTime updatedAt)
    {
        Id = id;
        BranchOfficeName = branchOfficeName;
        AdminName = adminName;
        PhoneNumber = phoneNumber;
        Address = address;
        Fax = fax;
        OrderPerMonth = orderPerMonth;
        CreatedAt = createdAt;
        UpdatedAt = updatedAt;
    }

    public static ErrorOr<BranchOfficeModel> Create(
        string branchOfficeName, string adminName, string phoneNumber, string address, string fax, int orderPerMonth, Guid? id = null)
    {
        List<Error> errors = new();

        if (branchOfficeName.Length is < MinBranchOfficeNameLength or > MaxBranchOfficeNameLength)
        {
            errors.Add(Errors.BranchOffice.InvalidBranchOfficeName);
        }

        if (adminName.Length is < MinAdminNameLength or > MaxAdminNameLength)
        {
            errors.Add(Errors.BranchOffice.InvalidAdminName);
        }

        if (errors.Count > 0)
        {
            return errors;
        }

        return new BranchOfficeModel(id ?? Guid.NewGuid(), branchOfficeName, adminName, phoneNumber, address, fax, orderPerMonth, DateTime.UtcNow, DateTime.UtcNow);
    }

    public static ErrorOr<BranchOfficeModel> From(CreateBranchOfficeRequest request) { return Create(request.BranchOfficeName, request.AdminName, request.PhoneNumber, request.Address, request.Fax, request.OrderPerMonth); }

    public static ErrorOr<BranchOfficeModel> From(Guid id, UpdateBranchOfficeRequest request) { return Create(request.BranchOfficeName, request.AdminName, request.PhoneNumber, request.Address, request.Fax, request.OrderPerMonth, id); }
}