using ErrorOr;
using BranchOffice.Models;

namespace BranchOffice.ServiceErrors;

public static class Errors
{
    public static class BranchOffice
    {
        public static Error InvalidBranchOfficeName => Error.Validation(code: "BranchOffice.InvalidBranchOfficeName", description: $"Branch Office name must be at least {BranchOfficeModel.MinAdminNameLength} characters long and at most {BranchOfficeModel.MaxBranchOfficeNameLength} characters long.");

        public static Error InvalidAdminName => Error.Validation(code: "BranchOffice.InvalidAdminName", description: $"Admin name must be at least {BranchOfficeModel.MinAdminNameLength} characters long and at most {BranchOfficeModel.MaxAdminNameLength} characters long.");

        public static Error NotFound => Error.NotFound(code: "BranchOffice.NotFound", description: "Branch office not found");
    }
}